﻿using NPoco;
using CrudNew.ResponseModels;
using Survey.DataAccess;
using LoginApp.Models;

namespace Survey.Api.Services
{
    public class UserService
    {
        //public UserResponse Get(int id)
        //{
        //    var cmd = Sql.Builder.Select("si.*, f.FileName as FileName, f.FileUrl as FileUrl").From("SurveyItems si");
        //    cmd.LeftJoin("Files f").On("f.Id = si.FileId");
        //    cmd.Where("si.Id=@0", id);
        //    using (var db = Utility.Database)
        //    {
        //        var result = db.FirstOrDefault<SurveyItemResponse>(cmd);
        //        return result;
        //    }
        //}

        public int AddUser(UserReq userRequest)
        {
            var checkQuery = @$"select exists(select * from users where username='{userRequest.UserName}') as UserCount";
            using (var dbse = Utility.Database)
            {
                UserAvailable checkRes = dbse.Fetch<UserAvailable>(checkQuery).FirstOrDefault();
                if (checkRes.UserCount == 0)
                {
                    var query = @$"insert into users(username,address,salt,password,cell)
                               values('{userRequest.UserName}','{userRequest.Address}','{userRequest.Salt}','{userRequest.Password}','{userRequest.Cell}')";
                    using (var db = Utility.Database)
                    {
                        var res = db.Execute(query);
                        return res;

                    }
                }
                else return 500;
                

            }

            
        }

        public List<UserResponse> GetAll()
        {
            var cmd = Sql.Builder.Select("*").From("Users");
            //cmd.LeftJoin("Files f").On("f.Id = si.FileId");
            //cmd.Where("si.SurveyId=@0", surveyId);
            //if (!string.IsNullOrWhiteSpace(query))
            //{
            //    cmd.Where("si.Content like @0", $"%{query}%");
            //}
            using (var db = Utility.Database)
            {
                var result = db.Fetch<UserResponse>(cmd);
                return result;
            }
           
            //cmd.OrderBy("si.Id");
            //using (var db = Utility.Database)
            //{
            //    var result = db.Page<SurveyItemResponse>(pageNumber, pageSize, cmd);
            //    return result;
            //}
        }


        public UserResponse GetUserById(int id)
        {
            var cmd = Sql.Builder.Select("*").From("Users");
            cmd.Where("users.id=@0", $"{id}");
            using (var db = Utility.Database)
            {
                var result = db.Fetch<UserResponse>(cmd).FirstOrDefault();
                return result;
            }
        }


        public int UpdateUser(UserRequest userRequest)
        {
            var query = @$"update users set username='{userRequest.UserName}',address='{userRequest.Address}',cell='{userRequest.Cell}' where id='{userRequest.Id}'";
            using (var db = Utility.Database)
            {
                var res = db.Execute(query);
                return res;

            }
        }

        public int DeleteUser(int id)
        {
            var query = @$"delete from users where id='{id}'";
            using (var db = Utility.Database)
            {
                var res = db.Execute(query);
                return res;

            }
        }

        public UserResponse CheckUserByUsername(string username)
        {
            var query = @$"select * from users where username='{username}'";
            using (var db = Utility.Database)
            {
                var res = db.Fetch<UserResponse>(query).FirstOrDefault();
                return res;

            }
        }




        //public object Save(SurveyItem surveyItem)
        //{
        //    using (var db = Utility.Database)
        //    {
        //        db.Save(surveyItem);
        //        return surveyItem;
        //    }
        //}
    }
}