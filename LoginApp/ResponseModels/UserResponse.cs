﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace CrudNew.ResponseModels
{

    public class UserRequest
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; }



        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }


        [Required(ErrorMessage = "Cell is required")]
        [DataType(DataType.PhoneNumber)]
        public string Cell { get; set; }
    }

    public class UserReq
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; }

        public string Salt { get; set; }


        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }


        [Required(ErrorMessage = "Cell is required")]
        [DataType(DataType.PhoneNumber)]
        public string Cell { get; set; }
    }

    public class UserResponse : UserRequest
    {

        public string Salt { get; set; }
    }

    public class UserLoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool HasError { get; set; }
    }

    public class UserAvailable
    {
        public int UserCount { get; set; }
    }
}