﻿using CrudNew.ResponseModels;
using LoginApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using SharedLibrary;
using Survey.Api.Services;
using System.Diagnostics;
using System.Security.Claims;

namespace LoginApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IConfiguration _Configuration;

        public HomeController(ILogger<HomeController> logger, IConfiguration Configuration)
        {
            _logger = logger;
            _Configuration = Configuration;
        }

        public IActionResult Index(int returnVal)
        {
            var res = new UserService().GetAll();
            return View(res);
            
        }

        [HttpGet("SignUp")]
        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost("SignUp")]
        public IActionResult SignUp(UserRequest userRequest)
        {
            var userReq = new UserReq()
            {
                UserName = userRequest.UserName,
                Address = userRequest.Address,
                Cell = userRequest.Cell,
                Id = userRequest.Id,
                Salt = AppEncryption.CreateSalt()

            };
            userReq.Password = AppEncryption.CreatePasswordHash(userRequest.Password,userReq.Salt);
            if (ModelState.IsValid)
            {
                var returnVal = new UserService().AddUser(userReq);
                TempData["message"] = returnVal;
                return View();
            }
            return View();
        }

        [HttpGet("UpdateUser")]
        public IActionResult UpdateUser(int id)
        {
           var dbUser= new UserService().GetUserById(id);
            return View(dbUser);
        }


        [HttpPost("UpdateUser")]
        public IActionResult UpdateUser(UserRequest userRequest)
        {
            var returnVal = new UserService().UpdateUser(userRequest);
            TempData["message"] = returnVal;
            return View();
        }

        public IActionResult DeleteUser(int id)
        {
            var returnValue = new UserService().DeleteUser(id);
            TempData["deleteUserResult"] = returnValue;
            return RedirectToAction(nameof(GetAllUsers));
        }

        [HttpGet("GetAllUsers")]
        public IActionResult GetAllUsers()
        {
            var res = new UserService().GetAll();
            return View(res);

        }

        [HttpGet("Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost("Login")]
        public IActionResult Login(UserLoginRequest userLoginRequest)
        {
            var dbUser = new UserService().CheckUserByUsername(userLoginRequest.UserName);
            if (dbUser != null)
            {
                var pass = AppEncryption.CreatePasswordHash(userLoginRequest.Password, dbUser.Salt);
                if (dbUser.Password.Equals(AppEncryption.CreatePasswordHash(userLoginRequest.Password, dbUser.Salt)))
                {
                    ClaimsIdentity identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                    identity.AddClaim(new Claim(AppClaimTypes.UserId, dbUser.Id.ToString()));
                    identity.AddClaim(new Claim(AppClaimTypes.UserName, dbUser.UserName));
                    identity.AddClaim(new Claim(AppClaimTypes.Address, dbUser.Address));
                    identity.AddClaim(new Claim(AppClaimTypes.Cell, dbUser.Cell));


                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                    HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties
                    {
                        AllowRefresh = true,
                        IsPersistent = true,
                        ExpiresUtc = DateTime.Now.AddHours(12)
                    });

                    return RedirectToAction(nameof(GetAllUsers));
                }
                else
                {
                    TempData["checkPassword"] = userLoginRequest.HasError = true;
                    return View();
                }

            }
            else
            {
               TempData["userPresent"]= userLoginRequest.HasError = true;
                return View();
            }
            
        }
    }
}