﻿using CrudNew.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using SharedLibrary;
using System.Security.Cryptography.Xml;

namespace LoginApp.Controllers
{
    [Route("controller")]
    public class AccountController : Controller
    {
        private IConfiguration _Configuration;

        public AccountController(IConfiguration Configuration)
        {
            _Configuration = Configuration;
        }

        [HttpGet("SignUp")]
        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost("SignUp")]
        public IActionResult SignUp(UserRequest userRequest)
        {
            //userRequest.Salt = AppEncryption.CreateSalt();
            //userRequest.Password = AppEncryption.CreatePasswordHash(userRequest.Password, userRequest.Salt);
            return View();
        }
    }
}
