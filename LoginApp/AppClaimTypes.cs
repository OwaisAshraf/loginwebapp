﻿using System.Security.Claims;

namespace LoginApp
{
    public sealed class AppClaimTypes
    {
        public const string UserId = nameof(UserId);

        public const string UserName = nameof(UserName);

        public const string Address = nameof(Address);

        public const string Cell = nameof(Cell);
    }
}
